﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
	public Slider healthSlider;
	[SerializeField] int _maxHealth;
	[SerializeField] int _healthFallRate;
	
	public int maxHealth { get { return _maxHealth; } }
	public int healthFallRate { get { return _healthFallRate; } }


	public Slider hungerSlider;
	[SerializeField] int _maxHunger;
	[SerializeField] int _hungerFallRate;

	public int maxHunger { get { return _maxHunger; } }
	public int hungerFallRate { get { return _hungerFallRate; } }


	public Slider thirstSlider;
	[SerializeField] int _maxThirst;
	[SerializeField] int _thirstFallRate;

	public int maxThirst { get { return _maxThirst; } }
	public int thirstFallRate { get { return _thirstFallRate; } }

	private void Start()
	{
		healthSlider.maxValue = maxHealth;
		healthSlider.value = maxHealth;

		hungerSlider.maxValue = maxHunger;
		hungerSlider.value = maxHunger;

		thirstSlider.maxValue = maxThirst;
		thirstSlider.value = maxThirst;
	}

	private void Update()
	{
		if (hungerSlider.value <= 0 && (thirstSlider.value <= 0))
		{
			healthSlider.value -= Time.deltaTime / healthFallRate * 2;
		}
		else if(hungerSlider.value <= 0 || (thirstSlider.value <= 0))
		{
			healthSlider.value -= Time.deltaTime / healthFallRate;
		}

		if(healthSlider.value == 0)
		{
			//Kill player	
		}


		//Hunger controller
		if (hungerSlider.value > 0)
		{
			hungerSlider.value -= Time.deltaTime / hungerFallRate;
		}
		else if (hungerSlider.value <= 0)
		{
			hungerSlider.value = 0;
		}
		else if (hungerSlider.value >= maxHunger)
		{
			hungerSlider.value = maxHunger;
		}


		//Thirst controller
		if (thirstSlider.value > 0)
		{
			thirstSlider.value -= Time.deltaTime / thirstFallRate;
		}
		else if (thirstSlider.value <= 0)
		{
			thirstSlider.value = 0;
		}
		else if (thirstSlider.value >= maxThirst)
		{
			thirstSlider.value = maxThirst;
		}


	}




}
