﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    #region
    //Singleton
    public static GameManager instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than 1 GameManager found!!");
            return;
        }
        instance = this;
    }
    #endregion
    //List for inventory
    public List<Item> inventoryConsumables = new List<Item>();
    /* 
     * Potential Other Lists (May need a rework)
    public List<Item> inventoryMaterials = new List<Item>();
    public List<Item> inventoryTools = new List<Item>();
    */

    //Equipped item slot
    //public InventorySlot[] equipSlot = new InventorySlot[1];
    public Item[] equippedItem = new Item[1];

    //Parent of all item slots
    public Transform inventorySlotHolder;

    //Parent of equipped item slot
    public Transform equipSlotHolder;

    Sprite emptyEquipIcon;
    public Transform unEquipButton;

    //A list to hold all current inventory slots
    //List<InventorySlot> slotList = new List<InventorySlot>();
    //A list cannot be used, forced to use array
    InventorySlot[] slots;

    //Array to hold the equipped item inventory slot
    InventorySlot[] equipSlot;

    //Once main list works, make additional lists and add
    //certain items to those lists only. 
    //Also use multiple lists on same page OR multiple pages 
    //So each list of items has their own page to avoid clutter
    //This is *OPTIONAL*

    //To show/hide Inventory Screen (should be managed elsewhere not in the player)
    public GameObject inventoryScreen;

    //Capped at 5 for testing
    private int maxItemCount = 5;
    //maybe some other stuff....?
    //Later feature (semi coded already): Stacking items. When picking up an item, loop through all slots, if the item exists in the slot, increase the amount value
    //Make sure to display the count on screen as well

    //Temp item
    Item tempItem;

    void Start()
    {
        //From the inventory slot holder, put all the slots childed to it, into the array (with components)
        //If dynamic slots are required, put this in UpdateUI and retool with events/delegates
        slots = inventorySlotHolder.GetComponentsInChildren<InventorySlot>();

        //Hook up the equipped item slot
        equipSlot = equipSlotHolder.GetComponentsInChildren<InventorySlot>();

        emptyEquipIcon = equipSlot[0].itemIcon.sprite;

        UpdateUI();
        UpdateEquipUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            ShowInventory();
        }
    }

    #region InventoryRegion
    public bool AddItem(Item theItem)
    {
        //Try adding the item
        Debug.Log("Adding item..." + theItem.name);
        
        //Check for duplicate items
        if (inventoryConsumables.Contains(theItem))
        {
            //If the inventory contains theItem you are picking up already
            Debug.Log("Duplicate item detected: " + theItem.name);
            //Store the duplicate item's index in an int
            int potentialDupeIndex = inventoryConsumables.IndexOf(theItem);
            Debug.Log("The item at index " + potentialDupeIndex + " is " + inventoryConsumables[potentialDupeIndex].name);
            //Increase the amount by 1
            inventoryConsumables[potentialDupeIndex].amount += 1;
            Debug.Log("The amount of water in index " + potentialDupeIndex + " is " + inventoryConsumables[potentialDupeIndex].amount);
            //Update the UI and return true to the player that an item was actually picked up
			UpdateUI();
            return true;
        }
        
        if (inventoryConsumables.Count < maxItemCount)
        {
            //Check the item's type and add to the correct inventory (later to be added)
            //If there is no duplicate item in the inventory. then increase amount and add it
            theItem.amount++;
            inventoryConsumables.Add(theItem);
        }
        else
        {
            //If inventory is full, refresh UI, return false to not cause the item's destruction
            Debug.Log("Inventory Full!");
			UpdateUI();
            return false;
        }
        Debug.Log("There are " + inventoryConsumables.Count + " items in your inventory");
        //*/
        UpdateUI();
        return true;
    }

    public void EquipItem(Item myItem)
    {
        //Put the item in the single index array of 'equipped item'
        equippedItem[0] = myItem;
        Debug.Log("Currently Equipped item is " + equippedItem[0].name + " and is of type: " + equippedItem[0].myType);
        //Make sure the player knows they are currently holding an item and refresh equip slot UI
        Player.instance.curEquippedItem = myItem;
        UpdateEquipUI();

    }

    void UpdateUI()
    {
        //Called when adding an item if inventory isnt full
        //Set Icon to item.icon etc etc
        //This should be on inventory slot individually rather than game manager
        Debug.Log("Updating Inventory UI");
        for (int i = 0; i < slots.Length; i++)
        {
            
            //If i is less than number of items in the list, add to UI from List
            if (i < inventoryConsumables.Count)
            {
                Debug.Log("Checking each slot. Currently on slot " + i);
                slots[i].AddItem(inventoryConsumables[i]);
            }
            //else clear the slot
            else
            {
                slots[i].ClearItem();
                //slots[i].amountImage.gameObject.SetActive(false);
            }
        }
    }

    void UpdateEquipUI()
    {
        //The equipped item slot should not have an amount value. Rather than break the prefab, hide the object instead
        equipSlot[0].amountImage.gameObject.SetActive(false);
        Debug.Log("Equipping Item");
        if (equippedItem[0] != null)
        {
            equipSlot[0].itemIcon.sprite = equippedItem[0].icon;
            unEquipButton.gameObject.SetActive(true);
        }
        else
        {
            equipSlot[0].itemIcon.sprite = emptyEquipIcon;
            unEquipButton.gameObject.SetActive(false);
        }
    }

    //Shows the inventory
    void ShowInventory()
    {
        if (inventoryScreen.activeSelf == false)
        {
            inventoryScreen.SetActive(true);
        }
        else if(inventoryScreen.activeSelf == true)
        {
            inventoryScreen.SetActive(false);
        }
    }

    #endregion
}
