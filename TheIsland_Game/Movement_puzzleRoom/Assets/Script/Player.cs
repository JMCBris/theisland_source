﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Sprites;
public class Player : MonoBehaviour
{
    #region
    //Singleton
    public static Player instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than 1 " + this.name + " found!!");
            return;
        }
        instance = this;
    }
    #endregion

    private float moveSpeed = 5f;
	public Transform movePoint;

	//public Transform movePoint { get; private set; }
  	public LayerMask whatStopsMovement;

    //Inventory Code starts here;
    //Can i pickup what i'm standing on?
    //If pickup is false, ignore interact code
    [SerializeField] bool pickup;

    //The pickupable thing i'm trying to pick up
    [SerializeField] Pickupable myPickup;
    Item _curEquippedItem;
    public Item curEquippedItem;

    //Inventory Code ends here;

    void Start()
    {
        //Make movePoint no longer a child of player
        movePoint.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        //Assign publicly equipped item to private variable
        curEquippedItem = GameManager.instance.equippedItem[0];
        _curEquippedItem = curEquippedItem;
        Move();

        //Interact Function Call
        if (Input.GetKeyDown(KeyCode.E))
        {
            Interact();
        }
	}

    void Move()
    {
        //Move player character towards movepoint
        transform.position = Vector3.MoveTowards(transform.position, movePoint.position, moveSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, movePoint.position) <= 0.05f)
        {

            if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 1)
            {
                if (!Physics2D.OverlapCircle(movePoint.position + new Vector3(Input.GetAxisRaw("Horizontal"), 0f, 0f), 0.2f, whatStopsMovement))
                    movePoint.position += new Vector3(Input.GetAxisRaw("Horizontal"), 0f, 0f);
            }
            else if (Mathf.Abs(Input.GetAxisRaw("Vertical")) == 1)
            {
                if (!Physics2D.OverlapCircle(movePoint.position + new Vector3(0f, Input.GetAxisRaw("Vertical"), 0f), 0.2f, whatStopsMovement))
                    movePoint.position += new Vector3(0f, Input.GetAxisRaw("Vertical"), 0f);
            }
        }
    }

    //Interact with world objects
    void Interact()
    {
        //If inside a trigger, and able to pickup something, prompt console
        if (pickup == true)
        {
            bool wasPickedUp;
            Debug.Log("I am picking up " + myPickup.item.name);

            wasPickedUp = GameManager.instance.AddItem(myPickup.item);
            //Add item to GameManager inventory system
            //Delete object from game world (potentially)
            if (wasPickedUp == true)
            {
                Destroy(myPickup.gameObject);
                Debug.Log("The Item has been picked up");
            }
            else
            {
                Debug.Log("The inventory was full, the item remains...");
            }

            //Display currently equipped item
            //curEquippedItem = GameManager.instance.equippedItem[0];
        }
    }

    //Trigger stuff/pickupable stuff
    void OnTriggerStay2D(Collider2D other)
    {
        Debug.Log("I'm in a trigger");
        if (other.tag == "Item")
        {
            //Debug.Log("I've found " + other.name);
            pickup = true;
            myPickup = other.gameObject.GetComponent<Pickupable>();
            Debug.Log("I am standing near " + myPickup.item.name);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log("I have left the trigger");
        pickup = false;
        myPickup = null;
    }
}
