﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pickupable : MonoBehaviour
{


    //Item Information Template stored in Scriptable Objects
    public Item item;
    //information within item is as follows:
    /* Item Name
     * Item Type
     * Inventory Icon
     * 
     * All are null by default, but can be changed when they are created
     * All accessed by item.*property* (Eg. item.name);
     */

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Testing retrieval of data
    public void Greetings()
    {
        Debug.Log("Hello, my name is " + item.name + " and I am an item of type: " + item.myType);
    }

    void OnTriggerEnter2D(Collider2D otherCol)
    {
        Greetings();
    }
}
