﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    Item curItem;
    public Image itemIcon;
    public Image amountImage;
    public Text amountText;

    public void AddItem(Item newItem)
    {
        curItem = newItem;
        itemIcon.sprite = newItem.icon;
        itemIcon.enabled = true;
        curItem.amount = newItem.amount;
        amountImage.enabled = true;
        amountText.text = curItem.amount.ToString();
    }

    public void ClearItem()
    {
        curItem = null;
        itemIcon.sprite = null;
        itemIcon.enabled = false;
        amountImage.enabled = false;
        amountText.text = null;
    }

    public void EquipItem()
    {
        if (curItem != null && curItem.myType.Equals(Item.ItemType.Equippable))
        {
            GameManager.instance.EquipItem(curItem);
            itemIcon.sprite = curItem.icon;
            itemIcon.enabled = true;
        }
    }

    public void UnEquipItem(Sprite mySprite)
    {
        GameManager.instance.equippedItem[0] = null;
        itemIcon.sprite = mySprite;
        itemIcon.enabled = true;
    }

    //Use item method
    //When UI is clicked, if item is of type consumable, if amount is 1 or more, reduce amount by 1
    //If amount reaches 0, clear item
    public void UseItem()
    {
        if (curItem != null)
        {
            if (curItem.amount >= 1 && curItem.myType == Item.ItemType.Consumable)
            {
                curItem.amount -= 1;
                amountText.text = curItem.amount.ToString();
                Debug.Log(curItem.amount);
                if (curItem.amount == 0)
                {
                    ClearItem();
                }
            }
        }
    }
}
