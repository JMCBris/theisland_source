﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    public enum ItemType { Consumable, Material, Equippable, Quest };

    //Scriptable objects are objects that basically hold data
    //They dont need to be in the scene to work
    //This script will function as a 'template' for all items
    new public string name = "New Item";
    public Sprite icon;
    public ItemType myType;
    public int amount;
}
